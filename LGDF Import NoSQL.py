# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 22:33:31 2017

@author: Lloyd Jackman
"""

import xmltodict
import os
import lxml.etree as ET
import json
from pymongo import MongoClient
from ftplib import FTP
import sys
import zipfile
from datetime import datetime as dt
from multiprocessing import Pool, Lock
from threading import Thread

#%%


def zip_file_job(file_name):
    local_filename = os.path.join(temp, file_name)
    with open(local_filename, 'wb') as f:
        ftp = FTP(ftp_site, user, password)
        ftp.cwd("/datafeeds/LipperGlobalDataFeed5/standardfeed")
        ftp.retrbinary('RETR ' + file_name, f.write)
        ftp.quit()
    open(log, 'a').write(str(dt.utcnow().strftime("%d-%m-%Y %H:%M")) + "\t FTP \t Downloaded " + file_name +"\n")
    xml_list = zipfile.ZipFile(local_filename).namelist()
    local_xml_list = []
    for xml_file in xml_list:
        if not xml_file.endswith(".xml"):
            continue
        local_xml_list.append(os.path.join(temp, xml_file))
    pool = Pool()
    zipfile.ZipFile(local_filename).extractall(path=temp)
    pool.map(sc_process, local_xml_list)
    pool.close()
    pool.join()
    os.remove(local_filename)


def sc_process(xml_file):
    file = xml_file[-12:]
    client = MongoClient()
    db = client.LGDF
    collection = db.ShareClass

    try:
        root = ET.parse(xml_file).getroot()
    except:
        print("Parsing error:" + xml_file)
        os.remove(xml_file)
    ns = root.tag[:-4]
    if file.startswith("1"):
        sc_str = ET.tostring(root, encoding='unicode', method='xml')
        lipperid = root.find("./%sAssetOverview" % ns).get("Id")
        dict_filter = {"Feed.AssetOverview.@Id": lipperid}
        try:
            sc_dict = dict(xmltodict.parse(sc_str))
            #pprint(sc_dict)
        except Exception as err:
            print(file, lipperid, "XML to Dict")
            print(str(err))
        try:
            collection.update_one(filter=dict_filter, update={"$set": sc_dict}, upsert=True)
        except Exception as err:
            print(file, lipperid, 'MongoDB Upsert')
            print(str(err))
        finally:
            try:
                os.remove(xml_file)
            except:
                pass
    for shareclass in root.findall("./%sAssetOverview/%sShareClasses/%sShareClass" % (ns, ns, ns)):
        lipperid = shareclass.get("Id")
        sc_str = ET.tostring(shareclass, encoding='unicode', method='xml')
        dict_filter = {"ShareClass.@Id": lipperid}
        try:
            sc_dict = dict(xmltodict.parse(sc_str))
        except Exception as err:
            print(file, lipperid, "XML to Dict")
            print(str(err))
        try:
            collection.update_one(filter=dict_filter, update={"$set": sc_dict}, upsert=True)
        except Exception as err:
            print(file, lipperid, 'MongoDB Upsert')
            print(str(err))
        finally:
            try:
                os.remove(xml_file)
            except:
                pass

#%%
if __name__ == '__main__':
    __spec__ = None
    #This first part sets up the variables, such as FTP credentials and the feed specifics
    with open(r'C:\Users\u0136211\Documents\LGDF\LGDFImportNoSQL\config.json', 'r') as config:
        data = json.load(config)
        ftp_site = data["ftp"]["site"]
        user = data["ftp"]["user"]
        password = data["ftp"]["password"]
        feed_series = data["feed"]["series"]
        feed_seq = data["feed"]["sequence"]
        feed_type = data["feed"]["type"]
        temp = data["temp"]
        log = data["log"]

    ftp = FTP(ftp_site, user, password)
    ftp.cwd("/datafeeds/LipperGlobalDataFeed5/standardfeed")
    open(log, 'a').write(str(dt.utcnow().strftime("%d-%m-%Y %H:%M")) + "\t FTP \t Logged in to " + ftp_site +"\n")
    folder_contents = list(ftp.mlsd())
    ftp.quit()

#%%
    #We then check for md5 checksum files, the last file to be delivered in LGDF deliveries, and create a list of batches to be processed

    batches_to_process = []
    for item in folder_contents:
        file_name = item[0]
        if not file_name.endswith(".md5"): continue
        if not file_name.split("_")[0] == feed_series: continue
        if not int((file_name.split("_")[3]).split(".")[0]) > feed_seq: continue
        if not file_name.split("_")[1] == feed_type: continue
        batch = file_name.split(".")[0]
        print(batch)
        batches_to_process.append(batch)
    if len(batches_to_process) == 0: sys.quit()
    batches_to_process = sorted(batches_to_process)

#%%
    #This next part downloads each of the zip files for each of the batches in turn, unzipping them, parsing their contents before writing them to a database

    for batch in batches_to_process:
        files_to_process = []
        for file in folder_contents:
            file_name = file[0]
            if not file_name.startswith(batch[:-4]):
                continue
            if not file_name.endswith(".zip"):
                continue
            if file[1]["size"] == 0:
                open(log, 'a').write(str(dt.utcnow().strftime("%d-%m-%Y %H:%M")) + "\t ERROR \t 0 byte file: " + file_name +"\n")
                continue
            local_filename = os.path.join(temp, file_name)
            with open(local_filename, 'wb') as f:
                ftp = FTP(ftp_site, user, password)
                ftp.cwd("/datafeeds/LipperGlobalDataFeed5/standardfeed")
                ftp.retrbinary('RETR ' + file_name, f.write)
                ftp.quit()
            open(log, 'a').write(str(dt.utcnow().strftime("%d-%m-%Y %H:%M")) + "\t FTP \t Downloaded " + file_name +"\n")
            xml_list = zipfile.ZipFile(local_filename).namelist()
            local_xml_list = []
            for xml_file in xml_list:
                if not xml_file.endswith(".xml"):
                    continue
                local_xml_list.append(os.path.join(temp, xml_file))
            pool = Pool()
            zipfile.ZipFile(local_filename).extractall(path=temp)
            pool.map(sc_process, local_xml_list)
            pool.close()
            pool.join()
            os.remove(local_filename)

#%%
    data_dict = {}
    data_dict["temp"] = temp
    data_dict["log"] = log
    data_dict["ftp"] = {}
    data_dict["ftp"]["site"] = ftp_site
    data_dict["ftp"]["user"] = user
    data_dict["ftp"]["password"] = password
    data_dict["feed"] = {}
    data_dict["feed"]["series"] = feed_series
    data_dict["feed"]["sequence"] = int(feed_seq)
    data_dict["feed"]["type"] = feed_type
    with open('config.json', 'w') as config:
        json.dump(data_dict, config)
