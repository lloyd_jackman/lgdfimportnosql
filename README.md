# README #

LGDF Import - NoSQL

### What is this repository for? ###

This script downloads Lipper Global Data Feed (LGDF) files and upserts them into a MongoDB database at a share class/index level.

### How do I get set up? ###

* Install MongoDB
* Install Python (better Anaconda) v3.x
* Ensure below libraries are accessible:

import xmltodict (pip install)

import os (standard library)

import lxml.etree as ET (standard library)

import json (standard library)

from pymongo import MongoClient (pip install)

from ftplib import FTP (standard library)

import sys (standard library)

import zipfile (standard library)

from datetime import datetime as dt (standard library)

### Who do I talk to? ###

lloyd.jackman@thomsonreuters.com